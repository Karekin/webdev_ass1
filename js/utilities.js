function changeTextColor(id, newColor){
    let tag = document.getElementById(id);
    tag.style.color = newColor;
}

function changeBackgroundColor(id, newColor){
    let tag = document.getElementById(id);
    tag.style.backgroundColor = newColor;
}

function changeTagWidth(id, newWidth){
    let tag = document.getElementById(id);
    tag.style.width = newWidth + "%";
}

function changeBorderColor(id, newColor){
    let tag = document.getElementById(id);
    tag.style.borderColor = newColor;
}

function changeBorderWidth(id, newWidth){
    let tag = document.getElementById(id);
    tag.style.borderWidth = newWidth + "px";
}
"uses strict";

document.addEventListener('DOMContentLoaded', generateTable );

function generateTable(){

    let old = document.getElementById("generatedTable");
    if(old){
        old.remove();
    }

    let inputRC = document.getElementById("row-count");
    let inputCC = document.getElementById("col-count");
    let inputTW = document.getElementById("table-width");
    let inputTC = document.getElementById("text-color");
    let inputBC = document.getElementById("background-color");
    let inputBW = document.getElementById("border-width");
    let inputBoC = document.getElementById("border-color");

    inputRC.addEventListener("blur", generateTable);
    inputCC.addEventListener("blur", generateTable);
    inputTW.addEventListener("blur", generateTable);
    inputTC.addEventListener("blur", generateTable);
    inputBC.addEventListener("blur", generateTable);
    inputBW.addEventListener("blur", generateTable);
    inputBoC.addEventListener("blur", generateTable);


    let table = document.createElement("table");
    table.style.width = inputTW.value + "%";
    table.setAttribute("border", inputBW.value);

    let tableBody = document.createElement("tbody");
    let count = 0;

    for(let rowCount = 0; rowCount < inputRC.value; rowCount++){
        let row = document.createElement("tr");
        for(let colCount = 0; colCount < inputCC.value; colCount++){
            count++;
            let cell = document.createElement("td");
            let cellHTML = document.createTextNode("cell" + count);
            cell.appendChild(cellHTML);
            row.appendChild(cell);
        }

        tableBody.appendChild(row);
    }
    table.appendChild(tableBody);
    let body = document.getElementById("table-render-space");
    body.appendChild(table);
    table.setAttribute("id", "generatedTable");

    changeTextColor("generatedTable", inputTC.value);
    changeBackgroundColor("generatedTable", inputBC.value);
    changeBorderColor("generatedTable", inputBoC.value);

    oldText = document.getElementById("tableText");
    if(oldText){
        oldText.remove();
    }

    let htmlRender = document.getElementById("table-html-space");
    let tr = document.querySelectorAll("tr");
    let finalText = document.createElement("textarea");
    let tableHTML = "<table>\n  ";
    for(let i = 0; i < inputRC.value; i++){
        tableHTML += "<tr>\n     ";
        tableHTML += tr[i].innerHTML +"\n   ";
        if(i == inputRC.value - 1){
            tableHTML += "</tr>\n";
        }else {
        tableHTML += "</tr>\n   ";
        }
    }
    tableHTML += "</table>";
    finalText.textContent = tableHTML;
    finalText.setAttribute("id", "tableText");
    htmlRender.append(finalText);
}